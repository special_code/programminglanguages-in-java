package languages;

public class ProgrammingLanguages implements Comparable<ProgrammingLanguages> {


    private String name;
    private Integer year;
    private Integer id;

    ProgrammingLanguages(String name, Integer year, Integer id) {
        this.name = name;
        this.year = year;
        this.id = id;

    }

    public String getName() {
        return this.name;
    }

    public Integer getYear() {
        return this.year;
    }

    public Integer getId() {
        return this.id;
    }

    @Override
    public String toString() {
        return this.getName() + "  " + "  " + this.getYear() + " " + this.getId();
    }

    @Override
    public int compareTo(ProgrammingLanguages pLanguages) {
        return (this.year - pLanguages.getYear() - pLanguages.getYear());

    }
}