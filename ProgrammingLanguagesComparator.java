package languages;

import languages.ProgrammingLanguages;

import java.util.Comparator;

public class ProgrammingLanguagesComparator implements Comparator<ProgrammingLanguages>{


   @Override
	public int compare(ProgrammingLanguages pLanguages1 ,ProgrammingLanguages pLanguages2){ 
		
		return pLanguages1.getId() - pLanguages2.getId();
	
	}

}