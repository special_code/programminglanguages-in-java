package languages;

import static java.lang.System.out;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class MainProgrammingLanguages {


    public static void main(String args[]) {


        List<ProgrammingLanguages> programmingLanguages = new ArrayList<>();

        programmingLanguages.add(new ProgrammingLanguages("Java", 1996, 2));
        programmingLanguages.add(new ProgrammingLanguages("JavaScript", 1995, 1));
        programmingLanguages.add(new ProgrammingLanguages("C Programming", 1972, 8));
        programmingLanguages.add(new ProgrammingLanguages("Cpp", 1979, 4));
        programmingLanguages.add(new ProgrammingLanguages("Python", 1992, 5));
        programmingLanguages.add(new ProgrammingLanguages("Lisp", 1958, 3));
        programmingLanguages.add(new ProgrammingLanguages("C#", 2000, 7));
        programmingLanguages.add(new ProgrammingLanguages("Ruby", 1995, 6));
        programmingLanguages.add(new ProgrammingLanguages("Php", 1995, 9));
        programmingLanguages.add(new ProgrammingLanguages("Fortran", 1954, 10));

        out.println("Default order");


        int amountLanguages = programmingLanguages.size();
        out.println("Number of programming languages " + amountLanguages);

        /*Ordenando usando comparator*/
        Collections.sort(programmingLanguages, new ProgrammingLanguagesComparator());
        out.println(programmingLanguages + " Comparator");

        /*Ordenando comparable*/
        Collections.sort(programmingLanguages);
        out.println(programmingLanguages + " comparable");

        /*Ordenando com expressao lambda*/
        programmingLanguages.sort((primeiro, segundo) -> primeiro.getId() - segundo.getId());
        out.println(programmingLanguages + " lambda");

        /*Ordenando com referencia */
        programmingLanguages.sort(Comparator.comparingInt(ProgrammingLanguages::getId));
        out.println(programmingLanguages + " reference ");

        /*Ordenando com referencia de metodo estatico*/
        programmingLanguages.sort(Comparator.comparingInt(ProgrammingLanguages::getId).reversed());
        out.println(programmingLanguages + " static method reversed()");

    }

}